package bdd.jdbc.model;

public class Supplier {
	private Integer idSupplier;
	private String adress;
	private String phone;
	private String email;
	private Integer idDeposit;
	public Integer getIdSupplier() {
		return idSupplier;
	}
	public void setIdSupplier(Integer idSupplier) {
		this.idSupplier = idSupplier;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getIdDeposit() {
		return idDeposit;
	}
	public void setIdDeposit(Integer idDeposit) {
		this.idDeposit = idDeposit;
	}
	@Override
	public String toString() {
		return "Supplier [idSupplier= " + idSupplier + ", adress= " + adress + ", phone= " + phone + ", email= " + email
				+ ", idDeposit= " + idDeposit + "]";
	}
	
}
