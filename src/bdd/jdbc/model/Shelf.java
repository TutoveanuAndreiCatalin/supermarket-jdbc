package bdd.jdbc.model;

public class Shelf {
	private Integer idShelf;
	private Boolean isFrigorific;
	private Integer shelfCapacity;
	private String  typeProduct;
	private Integer idDeposit;
	
	public Integer getIdShelf() {
		return idShelf;
	}
	public void setIdShelf(Integer idShelf) {
		this.idShelf = idShelf;
	}
	public Boolean getIsFrigorific() {
		return isFrigorific;
	}
	public void setIsFrigorific(Boolean isFrigorific) {
		this.isFrigorific = isFrigorific;
	}
	public Integer getShelfCapacity() {
		return shelfCapacity;
	}
	public void setShelfCapacity(Integer shelfCapacity) {
		this.shelfCapacity = shelfCapacity;
	}
	public String getTypeProduct() {
		return typeProduct;
	}
	public void setTypeProduct(String typeProduct) {
		this.typeProduct = typeProduct;
	}
	public Integer getIdDeposit() {
		return idDeposit;
	}
	public void setIdDeposit(Integer idDeposit) {
		this.idDeposit = idDeposit;
	}
	@Override
	public String toString() {
		return "Shelf [idShelf= " + idShelf + ", isFrigorific= " + isFrigorific + ", shelfCapacity = " + shelfCapacity
				+ ", typeProduct= " + typeProduct + ", idDeposit= " + idDeposit + "]";
	}
	
	
}
