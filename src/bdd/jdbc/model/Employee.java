package bdd.jdbc.model;
import java.sql.Date;

public class Employee {
	
	private Integer idEmployee;
	private String CNP;
	private String firstName;
	private String lastName;
	private Date date;
	private String adress;
	private String phone;
	private Integer idBudget;
	
	public Integer getIdEmployee() {
		return idEmployee;
	}
	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}
	public String getCNP() {
		return CNP;
	}
	public void setCNP(String cNP) {
		CNP = cNP;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Integer getIdBudget() {
		return idBudget;
	}
	public void setIdBudget(Integer idBudget) {
		this.idBudget = idBudget;
	}
}
