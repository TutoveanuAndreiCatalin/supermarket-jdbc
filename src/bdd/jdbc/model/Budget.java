package bdd.jdbc.model;

public class Budget {
	
	private Integer idBudget;
	private Integer cash;
	private Integer meintenance;
	private Integer salaries;
	
	public Integer getCash() {
		return cash;
	}
	public void setCash(Integer cash) {
		this.cash = cash;
	}	
	public Integer getIdBudget() {
		return idBudget;
	}
	public void setIdBudget(Integer idBudget) {
		this.idBudget = idBudget;
	}
	public Integer getMeintenance() {
		return meintenance;
	}
	public void setMeintenance(Integer meintenance) {
		this.meintenance = meintenance;
	}
	public Integer getSalaries() {
		return salaries;
	}
	public void setSalaries(Integer salaries) {
		this.salaries = salaries;
	}
	
}
