package bdd.jdbc.model;

public class SupermarketDetail {
	private Integer idSupermarketDetail;
	private String phone;
	private String adress;
	private String name;
	public Integer getIdSupermarketDetail() {
		return idSupermarketDetail;
	}
	public void setIdSupermarketDetail(Integer idSupermarketDetail) {
		this.idSupermarketDetail = idSupermarketDetail;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "SupermarketDetail [idSupermarketDetail= " + idSupermarketDetail + ", phone= " + phone + ", adress= "
				+ adress + ", name= " + name + "]";
	}
	
}
