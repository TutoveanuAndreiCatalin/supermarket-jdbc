package bdd.jdbc.model;

import java.sql.Date;

public class Product {

	private Integer idProduct;
	private Integer idSupplier;
	private Integer idDeposit;
	private String name;
	private String typeProduct;
	private Date expirationDate;
	private Date receiptDate;
	private Integer price;
	private Double TVA;
	private Integer idShelf;
	private Integer idReceipt;
	
	public Integer getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(Integer idProduct) {
		this.idProduct = idProduct;
	}
	public Integer getIdSupplier() {
		return idSupplier;
	}
	public void setIdSupplier(Integer idSupplier) {
		this.idSupplier = idSupplier;
	}
	public Integer getIdDeposit() {
		return idDeposit;
	}
	public void setIdDeposit(Integer idDeposit) {
		this.idDeposit = idDeposit;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTypeProduct() {
		return typeProduct;
	}
	public void setTypeProduct(String typeProduct) {
		this.typeProduct = typeProduct;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public Date getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Double getTVA() {
		return TVA;
	}
	public void setTVA(Double tVA) {
		TVA = tVA;
	}
	public Integer getIdShelf() {
		return idShelf;
	}
	public void setIdShelf(Integer idShelf) {
		this.idShelf = idShelf;
	}
	public Integer getIdReceipt() {
		return idReceipt;
	}
	public void setIdReceipt(Integer idReceipt) {
		this.idReceipt = idReceipt;
	}
	@Override
	public String toString() {
		return "Product [idProduct= " + idProduct + ", idSupplier= " + idSupplier + ", idDeposit= " + idDeposit + ", name= "
				+ name + ", typeProduct= " + typeProduct + ", expirationDate= " + expirationDate + ", receiptDate= "
				+ receiptDate + ", price= " + price + ", TVA= " + TVA + ", idShelf= " + idShelf + ", idReceipt= "
				+ idReceipt + "]";
	}
	
}
