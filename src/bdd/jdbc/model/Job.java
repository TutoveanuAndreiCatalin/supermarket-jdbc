package bdd.jdbc.model;

public class Job {
	private String title;
	private Integer salary;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getSalary() {
		return salary;
	}
	public void setSalary(Integer salary) {
		this.salary = salary;
	}
	
	public Job(String title, Integer salary) {
		this.title = title;
		this.salary = salary;
	}
	public Job() {
		
	}
	@Override
	public String toString() {
		return "Job [title=" + title + ", salary=" + salary + "]";
	}
	
}
