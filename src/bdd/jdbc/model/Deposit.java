package bdd.jdbc.model;

public class Deposit {
	private Integer idDeposit;
	private Integer noUnits;
	private Integer capacity;
	public Integer getIdDeposit() {
		return idDeposit;
	}
	public void setIdDeposit(Integer idDeposit) {
		this.idDeposit = idDeposit;
	}
	public Integer getNoUnits() {
		return noUnits;
	}
	public void setNoUnits(Integer noUnits) {
		this.noUnits = noUnits;
	}
	public Integer getCapacity() {
		return capacity;
	}
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}
	@Override
	public String toString() {
		return "Deposit [idDeposit= " + idDeposit + ", noUnits= " + noUnits + ", capacity= " + capacity + "]";
	}
	
}
