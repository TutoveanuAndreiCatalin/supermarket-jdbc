package bdd.jdbc.model;

public class Receipt {
	private Integer idReceipt;
	private Double totalPrice;
	private Integer idBudget;
	private Integer idSupermarketDetails;
	public Integer getIdReceipt() {
		return idReceipt;
	}
	public void setIdReceipt(Integer idReceipt) {
		this.idReceipt = idReceipt;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Integer getIdBudget() {
		return idBudget;
	}
	public void setIdBudget(Integer idBudget) {
		this.idBudget = idBudget;
	}
	public Integer getIdSupermarketDetails() {
		return idSupermarketDetails;
	}
	public void setIdSupermarketDetails(Integer idSupermarketDetails) {
		this.idSupermarketDetails = idSupermarketDetails;
	}
	@Override
	public String toString() {
		return "Receipt [idReceipt= " + idReceipt + ", totalPrice= " + totalPrice + ", idBudget= " + idBudget
				+ ", idSupermarketDetails= " + idSupermarketDetails + "]";
	}
	
}
