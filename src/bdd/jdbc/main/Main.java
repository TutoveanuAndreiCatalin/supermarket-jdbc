package bdd.jdbc.main;

import java.util.List;
import java.util.Scanner;
import bdd.jdbc.database.Database;
import bdd.jdbc.database.DatabaseConnection;
import bdd.jdbc.model.Job;
import bdd.jdbc.util.JobOperation;

public class Main {
	static Scanner console = new Scanner(System.in);
	public static void emptyLines() {
		for(int i=0;i<30;i++)
		System.out.println();
	}
	public static Integer menu() {
		System.out.println("<<<<<<<<<< Menu >>>>>>>>>>");
		System.out.println("1.Create a job");
		System.out.println("2.Print lists of jobs");
		System.out.println("3.Delete a job");
		System.out.print("Select option you want: ");
		Integer option = console.nextInt();
		return option;
	}

	public static void createAJob() {
		System.out.println("You nedd to add a name of job and salary");
		System.out.print("Job title: ");
		String title = console.next();
		System.out.print("Job salary (should be integer): ");
		Integer salary = console.nextInt();

		DatabaseConnection dc = makeConnection();
		JobOperation jobOperation = new JobOperation();
		jobOperation.jobOperation(dc);

		System.out.println("Before adding...");
		List<Job> jobs = jobOperation.listOfJobs();
		jobOperation.printListOfJobs(jobs);

		Job jobToAdd = new Job(title, salary);

		jobOperation.createAJob(jobToAdd);

		System.out.println("After adding...");
		jobs = jobOperation.listOfJobs();
		jobOperation.printListOfJobs(jobs);
	}

	public static void printListOfJobs() {
		DatabaseConnection dc = makeConnection();
		JobOperation jobOperation = new JobOperation();
		jobOperation.jobOperation(dc);
		List<Job> jobs = jobOperation.listOfJobs();
		jobOperation.printListOfJobs(jobs);
	}

	public static void deleteAJob() {
		DatabaseConnection dc = makeConnection();
		JobOperation jobOperation = new JobOperation();
		jobOperation.jobOperation(dc);
		System.out.println("You need to add a name of job");
		System.out.print("Job title: ");
		String title = console.next();		
		jobOperation.deleteAJob(title);
	}

	public static void optionSwitch(Integer option) {
		while (option != 0) {
			option = menu();
			switch (option) {
			case 1: {
				createAJob();
				emptyLines();
				break;
			}
			case 2: {
				printListOfJobs();
				emptyLines();
				break;
			}
			case 3: {
				deleteAJob();
				emptyLines();
				break;
			}
			default: {
				System.out.println("You've exited application!!");
				System.exit(0);
			}
			}
		}
	}

	public static DatabaseConnection makeConnection() {
		Database database = new Database("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/Supermarket", "root","ILoveShaorma19051999");
		DatabaseConnection databaseConnection = new DatabaseConnection(database);
		return databaseConnection;
	}

	public static void main(String args[]) {

		optionSwitch(1);
	}
}
