package bdd.jdbc.util;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import bdd.jdbc.database.DatabaseConnection;
import bdd.jdbc.model.Job;

public class JobOperation {
	private DatabaseConnection databaseConnection;

	public void jobOperation(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}
		
	public List<Job> listOfJobs(){
		databaseConnection.createConnection();
		String query = "SELECT title, salary FROM job";
		List<Job> jobs = new ArrayList<Job>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Job job = new Job();
				job.setTitle(rs.getString("title"));
				job.setSalary(rs.getInt("salary"));
				jobs.add(job);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return jobs;	
	}
	public void printListOfJobs(List<Job> jobs) {
		for (Job job : jobs) {
			System.out.println(job.toString());
		}
	}
	
	//CRUD For Job
	public void createAJob(Job job) {
		databaseConnection.createConnection();
		String query = "INSERT INTO job VALUES (?, ?)";
		PreparedStatement ps = null;
		try{
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setString(1, job.getTitle());
			ps.setInt(2,job.getSalary());
			ps.executeUpdate();
		}catch(SQLException e){
		System.err.println("Error when creating query: " + e.getMessage());
	}
	finally
	{
		try {
			ps.close();
			databaseConnection.getConnection().close();
		} catch (SQLException e) {
			System.err.println("Failed closing streams: " + e.getMessage());
		}
	}
	}
	public void readAJob(String title) {
		databaseConnection.createConnection();
		String query = "SELECT *FROM job WHERE title = $title";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Job job = new Job();
		try{
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			job.setSalary(rs.getInt("salary"));
			job.setTitle(rs.getString("title"));
		}catch(SQLException e){
		System.err.println("Error when creating query: " + e.getMessage());
	}
	finally
	{
		if(job.getSalary()!=null && job.getTitle()!="")
			System.out.println(job.toString());
		try {
			ps.close();
			databaseConnection.getConnection().close();
		} catch (SQLException e) {
			System.err.println("Failed closing streams: " + e.getMessage());
		}
	}
	}
	public void updateAJob(Job job, String oldTitle) {
		databaseConnection.createConnection();
		String query = "SELECT title FROM job";
		//String query = "SELECT FROM job WHERE title = \"" + oldTitle + "\" ";
		String query2 = "INSERT INTO job VALUES (?, ?)";
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps2 = null;
 		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps2 = databaseConnection.getConnection().prepareStatement(query2);
			rs = ps.executeQuery();
			while(rs.next()) {
				if(oldTitle.equals(rs.getString("title"))){
					ps2.setString(1, job.getTitle());
					ps2.setInt(2, job.getSalary());
					ps2.executeUpdate();
				}
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}
	public void deleteAJob(String title) {
		databaseConnection.createConnection();		
		String query = "DELETE FROM job WHERE title=\"" + title + "\"";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}

}
