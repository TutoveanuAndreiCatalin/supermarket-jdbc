package bdd.jdbc.util;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import bdd.jdbc.database.DatabaseConnection;
import bdd.jdbc.model.Receipt;

public class ReceiptOperation {
	private DatabaseConnection databaseConnection;

	public void receiptOperation(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}
	@SuppressWarnings("unused")
	private boolean addReceiptOperation(Receipt receipt) {
		databaseConnection.createConnection();
		String query = "INSERT INTO receipt VALUES (?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1,receipt.getIdReceipt());
			ps.setDouble(2,receipt.getTotalPrice());
			ps.setInt(3,receipt.getIdBudget());
			ps.setInt(4,receipt.getIdSupermarketDetails());
			ps.executeUpdate();
		}catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return true;
	}
	@SuppressWarnings("unused")
	private List<Receipt> listOfReceipts(){
		databaseConnection.createConnection();
		String query = "SELECT idReceipt, totalPrice, idBudget, idSupermarketDetails, FROM receipt";
		List<Receipt> receipts = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
			Receipt receipt = new Receipt();
			receipt.setIdReceipt(rs.getInt("idReceipt"));
			receipt.setTotalPrice(rs.getDouble("totalPrice"));
			receipt.setIdBudget(rs.getInt("idBudget"));
			receipt.setIdSupermarketDetails(rs.getInt("idSupermarketDetails"));
			receipts.add(receipt);
			}		
		}catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return receipts;
	}
	@SuppressWarnings("unused")
	private void printListOfReceipts(List<Receipt> receipts) {
		for(Receipt receipt: receipts) {
			System.out.println(receipt.toString());
		}
	}
}
