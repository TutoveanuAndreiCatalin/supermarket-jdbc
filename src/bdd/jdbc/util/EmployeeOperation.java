package bdd.jdbc.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bdd.jdbc.database.DatabaseConnection;
import bdd.jdbc.model.Employee;
import bdd.jdbc.model.EmployeeJob;

public class EmployeeOperation {

	private DatabaseConnection databaseConnection;

	public void employeeJobOperation(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	private boolean addEmployee(Employee employee) {
		databaseConnection.createConnection();
		String query = "INSERT INTO job VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, employee.getIdEmployee());
			ps.setString(2, employee.getCNP());
			ps.setString(3, employee.getFirstName());
			ps.setString(4, employee.getLastName());
			ps.setDate(5, employee.getDate());
			ps.setString(6, employee.getAdress());
			ps.setString(7, employee.getPhone());
			ps.setInt(8, employee.getIdBudget());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return true;
	}

	private List<Employee> getListOfEmployee() {
		databaseConnection.createConnection();
		String query = "SELECT idEmployee, CNP, firstName, lastName, date, adress, phone, idBudget FROM employee";
		List<Employee> employeeList = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Employee employee = new Employee();
				employee.setIdEmployee(rs.getInt("idEmployee"));
				employee.setCNP(rs.getString("CNP"));
				employee.setFirstName(rs.getString("firstName"));
				employee.setLastName(rs.getString("lastName"));
				employee.setDate(rs.getDate("date"));
				employee.setAdress(rs.getString("adress"));
				employee.setPhone(rs.getString("phone"));
				employee.setIdBudget(rs.getInt("idBudget"));
				employeeList.add(employee);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
			return employeeList;
		}
	private void printListOfEmployes(List<Employee> listOfEmployes) {
		for(Employee e: listOfEmployes) {
			System.out.println(e.toString());
		}
	}
}
