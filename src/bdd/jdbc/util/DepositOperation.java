package bdd.jdbc.util;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import bdd.jdbc.database.DatabaseConnection;
import bdd.jdbc.model.Deposit;
public class DepositOperation {

	private DatabaseConnection databaseConnection;
	public void BudgetOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}
	public boolean addDeposit(Deposit deposit) {
		databaseConnection.createConnection();
		String query = "INSERT INTO budget VALUES (?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, deposit.getIdDeposit());
			ps.setInt(2, deposit.getNoUnits());
			ps.setInt(3, deposit.getCapacity());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}		
		return true;
	}
	private void printDeposit(Deposit deposit) {
		System.out.println(deposit.toString());
	}
	
	//CRUD Deposit
	@SuppressWarnings("unused")
	private void createDeposit(Deposit deposit) {
		databaseConnection.createConnection();
		String query = "INSERT INTO deposit VALUES (?, ?, ?)";
		PreparedStatement ps = null;
		try{
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, deposit.getIdDeposit());
			ps.setInt(2,deposit.getNoUnits());
			ps.setInt(2,deposit.getCapacity());
			ps.executeUpdate();
		}catch(SQLException e){
		System.err.println("Error when creating query: " + e.getMessage());
	}
	finally
	{
		try {
			ps.close();
			databaseConnection.getConnection().close();
		} catch (SQLException e) {
			System.err.println("Failed closing streams: " + e.getMessage());
		}
	}
	}
	@SuppressWarnings("unused")
	private void readDeposit(Integer idDeposit) {
		databaseConnection.createConnection();
		String query = "SELECT FROM supermarketDetail WHERE idDepoit = \"" + idDeposit + "\" ";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Deposit deposit = new Deposit();
		try{
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			deposit.setIdDeposit(rs.getInt("idDeposit"));
			deposit.setNoUnits(rs.getInt("noUnits"));
			deposit.setCapacity(rs.getInt("capacity"));
		}catch(SQLException e){
		System.err.println("Error when creating query: " + e.getMessage());
	}
	finally
	{
		if(deposit.getIdDeposit()!=null)
			System.out.println(deposit.toString());
		try {
			ps.close();
			databaseConnection.getConnection().close();
		} catch (SQLException e) {
			System.err.println("Failed closing streams: " + e.getMessage());
		}
	}
	}
	@SuppressWarnings("unused")
	private void updateDeposit(Deposit deposit, Integer idOldDeposit) {
		databaseConnection.createConnection();
		String query = "SELECT title FROM deposit";
		//String query = "SELECT FROM job WHERE title = \"" + oldTitle + "\" ";
		String query2 = "INSERT INTO deposit VALUES (?, ?, ?)";
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps2 = null;
 		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps2 = databaseConnection.getConnection().prepareStatement(query2);
			rs = ps.executeQuery();
			while(rs.next()) {
				if(idOldDeposit.equals(rs.getInt("idDeposit"))){
					ps2.setInt(1, deposit.getIdDeposit());
					ps2.setInt(2, deposit.getNoUnits());
					ps2.setInt(3, deposit.getCapacity());
					ps2.executeUpdate();
				}
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}
	@SuppressWarnings("unused")
	private void deleteDeposit(Integer idDeposit) {
		databaseConnection.createConnection();		
		String query = "SELECT FROM deposit WHERE idDeposit= \"" + idDeposit + "\"";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}
	
}