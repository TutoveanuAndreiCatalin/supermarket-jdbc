package bdd.jdbc.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import bdd.jdbc.database.DatabaseConnection;
import bdd.jdbc.model.Product;

public class ProductOperation {
	private DatabaseConnection databaseConnection;

	public void employeeJobOperation(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}
	@SuppressWarnings("unused")
	private boolean addProduct(Product product) {
		databaseConnection.createConnection();
		String query = "INSERT INTO product VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, product.getIdDeposit());
			ps.setInt(2, product.getIdSupplier());
			ps.setInt(3, product.getIdDeposit());
			ps.setString(4, product.getName());
			ps.setString(5, product.getTypeProduct());
			ps.setDate(6, product.getExpirationDate());
			ps.setDate(7, product.getReceiptDate());
			ps.setInt(8, product.getPrice());
			ps.setDouble(9, product.getTVA());
			ps.setInt(10, product.getIdShelf());
			ps.setInt(11, product.getIdReceipt());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return true;
	}
	@SuppressWarnings("unused")
	private List<Product> listOfProducts(){
		databaseConnection.createConnection();
		String query = "SELECT idProduct, idSupplier, idDeposit, name, typeProduct, expirationDate, receiptDate, price, TVA, idShelf, idReceipt FROM product";
		List<Product> products = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Product product = new Product();
				product.setIdDeposit(rs.getInt("idDeposit"));
				product.setIdSupplier(rs.getInt("salary"));
				product.setIdDeposit(rs.getInt("idDeposit"));
				product.setName(rs.getString("name"));
				product.setTypeProduct(rs.getString("typeProduct"));
				product.setExpirationDate(rs.getDate("expirationDate"));
				product.setReceiptDate(rs.getDate("receiptDate"));
				product.setPrice(rs.getInt("price"));
				product.setTVA(rs.getDouble("TVA"));
				product.setIdShelf(rs.getInt("idShelf"));
				product.setIdReceipt(rs.getInt("idReceipt"));
				products.add(product);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return products;	
	}
	@SuppressWarnings("unused")
	private void printListOfProducts(List<Product> products) {
		for(Product product: products) {
			System.out.println(product.toString());
		}
	}
}
