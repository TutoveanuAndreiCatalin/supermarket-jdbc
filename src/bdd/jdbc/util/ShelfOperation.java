package bdd.jdbc.util;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bdd.jdbc.database.DatabaseConnection;
import bdd.jdbc.model.Receipt;
import bdd.jdbc.model.Shelf;

public class ShelfOperation {
	
	private DatabaseConnection databaseConnection;
	public void shelfOperation(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}
	private boolean addShelfOperation(Shelf shelf) {
		databaseConnection.createConnection();
		String query = "INSERT INTO shelf VALUES (?,?,?,?,?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1,shelf.getIdShelf());
			ps.setBoolean(2, shelf.getIsFrigorific());
			ps.setInt(3, shelf.getShelfCapacity());
			ps.setString(4,shelf.getTypeProduct());
			ps.setInt(5, shelf.getIdDeposit());
		}catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return true;
	}
	private List<Shelf> listOfShelf(){
		databaseConnection.createConnection();
		String query = "SELECT idShelf, isFrigorific, shelfCapacity, typeProduct, idDeposit FROM shelf";
		List<Shelf> shelfs = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Shelf shelf = new Shelf();
				shelf.setIdShelf(rs.getInt("idShelf"));
				shelf.setIsFrigorific(rs.getBoolean("isFrigorific"));
				shelf.setShelfCapacity(rs.getInt("shelfCapacity"));
				shelf.setTypeProduct(rs.getString("typeProduct"));
				shelf.setIdDeposit(rs.getInt("idDeposit"));
				shelfs.add(shelf);
			}
		}catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return shelfs;
	}
	private void printListOfShelfs(List<Shelf> shelfs) {
		for(Shelf shelf: shelfs) {
			System.out.println(shelf.toString());
		}
	}
}
