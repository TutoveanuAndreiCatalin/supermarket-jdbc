package bdd.jdbc.util;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import bdd.jdbc.database.DatabaseConnection;
import bdd.jdbc.model.SupermarketDetail;

public class SupermarketDetailOperation {
	private DatabaseConnection databaseConnection;

	public void employeeJobOperation(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}
	@SuppressWarnings("unused")
	private boolean addSupermarketDetail(SupermarketDetail supermarketDetail ) {
		databaseConnection.createConnection();
		String query = "INSERT INTO product VALUES (?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, supermarketDetail.getIdSupermarketDetail());
			ps.setString(2,supermarketDetail.getPhone());
			ps.setString(3, supermarketDetail.getAdress());
			ps.setString(4, supermarketDetail.getName());
			ps.executeUpdate();
		}catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return true;
	}
	@SuppressWarnings("unused")
	private void printSupermarketDetails(List<SupermarketDetail> supermarketDetails) {
		for(SupermarketDetail supermarketDetail : supermarketDetails) {
			System.out.println(supermarketDetail.toString());
		}
	}

	//CRUD For SupermarketDetails
	@SuppressWarnings("unused")
	private void createSupermarketDetails(SupermarketDetail supermarketDetails) {
		databaseConnection.createConnection();
		String query = "INSERT INTO supermarketDetail VALUES (?, ?, ?, ?)";
		PreparedStatement ps = null;
		try{
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, supermarketDetails.getIdSupermarketDetail());
			ps.setString(2,supermarketDetails.getPhone());
			ps.setString(3,supermarketDetails.getAdress());
			ps.setString(4,supermarketDetails.getName());
			ps.executeUpdate();
		}catch(SQLException e){
		System.err.println("Error when creating query: " + e.getMessage());
	}
	finally
	{
		try {
			ps.close();
			databaseConnection.getConnection().close();
		} catch (SQLException e) {
			System.err.println("Failed closing streams: " + e.getMessage());
		}
	}
	}
	@SuppressWarnings("unused")
	private void readSupermarketDetails(Integer idSupermarketDetails) {
		databaseConnection.createConnection();
		String query = "SELECT FROM supermarketDetail WHERE idSupermarketDetail = \"" + idSupermarketDetails + "\" ";
		PreparedStatement ps = null;
		ResultSet rs = null;
		SupermarketDetail supermarketDetail = new SupermarketDetail();
		try{
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			supermarketDetail.setIdSupermarketDetail(rs.getInt("idSupermarketDetail"));			
			supermarketDetail.setAdress(rs.getString("adress"));
			supermarketDetail.setName(rs.getString("name"));
			supermarketDetail.setPhone(rs.getString("phone"));
		}catch(SQLException e){
		System.err.println("Error when creating query: " + e.getMessage());
	}
	finally
	{
		if(supermarketDetail.getIdSupermarketDetail()!=null)
			System.out.println(supermarketDetail.toString());
		try {
			ps.close();
			databaseConnection.getConnection().close();
		} catch (SQLException e) {
			System.err.println("Failed closing streams: " + e.getMessage());
		}
	}
	}
	@SuppressWarnings("unused")
	private void updateSupermarketDetails(SupermarketDetail supermarketDetails,Integer idSupermarketDetails) {
		databaseConnection.createConnection();
		String query = "SELECT title FROM supermarketDetail";
		//String query = "SELECT FROM job WHERE title = \"" + oldTitle + "\" ";
		String query2 = "INSERT INTO supermarketDetail VALUES (?, ?)";
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps2 = null;
 		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps2 = databaseConnection.getConnection().prepareStatement(query2);
			rs = ps.executeQuery();
			while(rs.next()) {
				if(idSupermarketDetails.equals(rs.getInt("idSupermarketDetail"))){
					ps2.setInt(1, supermarketDetails.getIdSupermarketDetail());
					ps2.setString(2, supermarketDetails.getAdress());
					ps2.setString(3, supermarketDetails.getName());
					ps2.setString(4, supermarketDetails.getPhone());
					ps2.executeUpdate();
				}
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}
	@SuppressWarnings("unused")
	private void deleteSupermarketDetail(Integer idSupermarketDetail) {
		databaseConnection.createConnection();		
		String query = "DELETE FROM supermarketDetail WHERE idSupermarketDetail=\"" + idSupermarketDetail + "\" ";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.executeUpdate();
			}
		catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
	}
}
