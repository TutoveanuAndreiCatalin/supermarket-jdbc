package bdd.jdbc.util;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import bdd.jdbc.database.DatabaseConnection;
import bdd.jdbc.model.Budget;

public class BudgetOperation {
	
	private DatabaseConnection databaseConnection;
	public boolean addBudget(Budget budget) {
		databaseConnection.createConnection();
		String query = "INSERT INTO budget VALUES (?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, budget.getIdBudget());
			ps.setInt(2, budget.getCash());
			ps.setInt(3, budget.getMeintenance());
			ps.setInt(4, budget.getSalaries());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}		
		return true;
	}
	public void BudgetOperations(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}
	public void printBudget(Budget budget) {
		System.out.println("Available money: " + budget.getCash() + 
		"budget id: "+budget.getIdBudget() + "salaries: "+ budget.getSalaries() +
		"meintenance costs: "+ budget.getMeintenance());
	}
}
