package bdd.jdbc.util;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import bdd.jdbc.database.DatabaseConnection;
import bdd.jdbc.model.Supplier;

public class SupplierOperation {
	private DatabaseConnection databaseConnection;

	public void supplierOperation(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}

	private boolean addSupplier(Supplier supplier) {
		databaseConnection.createConnection();
		String query = "INSERT INTO supplier VALUES (?, ?, ?, ?, ?)";
		PreparedStatement ps = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, supplier.getIdSupplier());
			ps.setString(2, supplier.getAdress());
			ps.setString(3, supplier.getPhone());
			ps.setString(4, supplier.getEmail());
			ps.setInt(5, supplier.getIdDeposit());
			ps.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
			return false;
		} finally {
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return true;
	}

	private List<Supplier> listOfSupplier() {
		databaseConnection.createConnection();
		String query = "SELECT idSupplier, adress phone, email, idDeposit FROM supplier";
		List<Supplier> suppliers = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Supplier supplier = new Supplier();
				supplier.setIdSupplier(rs.getInt("idSupplier"));
				supplier.setAdress(rs.getString("adress"));
				supplier.setPhone(rs.getString("phone"));
				supplier.setEmail(rs.getString("email"));
				supplier.setIdDeposit(rs.getInt("idDeposit"));
				suppliers.add(supplier);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return suppliers;
	}
	private void printListOfSuppliers(List<Supplier> suppliers) {
		for(Supplier supplier : suppliers) {
			System.out.println(supplier.toString());
		}
	}
}
