package bdd.jdbc.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import bdd.jdbc.database.DatabaseConnection;
import bdd.jdbc.model.EmployeeJob;

public class EmployeeJobOperation {
	
	private DatabaseConnection databaseConnection;
	public void employeeJobOperation(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}
	public boolean addEmployeeJob(EmployeeJob employeeJob) {
		databaseConnection.createConnection();
		String query = "INSERT INTO job VALUES (?, ?)";
		PreparedStatement ps = null;
		try{
			ps = databaseConnection.getConnection().prepareStatement(query);
			ps.setInt(1, employeeJob.getIdEmployee());
			ps.setString(2,employeeJob.getJobTitle());
			ps.executeUpdate();
		}catch(SQLException e){
		System.err.println("Error when creating query: " + e.getMessage());
		return false;
		}
		finally
		{
			try {
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
			return true;
	}
	public List<EmployeeJob> listOfEmployeeJob(){
		databaseConnection.createConnection();
		String query = "SELECT idEmployee, jobTitle FROM employeeJob";
		List<EmployeeJob> employeeJobs = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = databaseConnection.getConnection().prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				EmployeeJob employeeJob = new EmployeeJob();
				employeeJob.setIdEmployee(rs.getInt("idEmployee"));
				employeeJob.setJobTitle(rs.getString("jobTitle"));
				employeeJobs.add(employeeJob);
			}
		} catch (SQLException e) {
			System.err.println("Error when creating query: " + e.getMessage());
		} finally {
			try {
				rs.close();
				ps.close();
				databaseConnection.getConnection().close();
			} catch (SQLException e) {
				System.err.println("Failed closing streams: " + e.getMessage());
			}
		}
		return employeeJobs;	
	}
}
